
"""
Demonstration module for quadratic interpolation.

Quadratic Interpolation
Modified by: ** Kevin Siswandi **
"""


import numpy as np
import matplotlib.pyplot as plt
from numpy.linalg import solve

def quad_interp(xi,yi):
    """
    Quadratic interpolation.  Compute the coefficients of the polynomial
    interpolating the points (xi[i],yi[i]) for i = 0,1,2.
    Returns c, an array containing the coefficients of
      p(x) = c[0] + c[1]*x + c[2]*x**2.

    """

    # check inputs and print error message if not valid:

    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==3 and len(yi)==3, error_message

    # Set up linear system to interpolate through data points:

    A = np.vstack([np.ones(3), xi, xi**2]).T

    c = solve(A, yi)

    return c


def plot_quad(xi, yi):
    """
    1. Takes two numpy arrays xi and yi of length 3,
    2. calls quad_interp to compute c, and then
    3. Plots both the interpolating polynomial and the data points, and
    4. saves the resulting figure as quadratic.png.
    """
    c = quad_interp(xi, yi)

    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)

    y = c[0] + c[1] * x + c[2] * x**2

    plt.figure(1)
    plt.clf()
    plt.plot(x, y, 'b-')

    plt.plot(xi, yi, 'ro')
    plt.title("Data points and interpolating polynomial")
    plt.savefig("quadratic.png")


def cubic_interp(xi, yi):
    
    # check inputs and print error message if not valid:                                                                  
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message

    error_message = "xi and yi should have length 3"
    assert len(xi)==4 and len(yi)==4, error_message

    # Set up linear system to interpolate through data points:                                                            

    A = np.vstack([np.ones(4), xi, xi**2, xi**3]).T

    c = solve(A, yi)

    return c


def poly_interp(xi, yi):
    #check inputs and print error message if invalid:
    error_message = "xi and yi should have type numpy.ndarray"
    assert (type(xi) is np.ndarray) and (type(yi) is np.ndarray), error_message
    error_message = "xi and yi should have the same length"
    assert len(xi)==len(yi), error_message

    n = len(xi)
    
    A = np.ones((n, n))
    for i in range(1, n):
        A[:, i] = xi**i

    c = solve(A, yi)

    return c


def plot_poly(xi, yi):
    
    c = poly_interp(xi, yi)
    
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)
    
    n = len(xi)

    #Apply Horner's rule:
    y = c[n - 1]
    for j in range(n - 1, 0, -1):
        y = y * x + c[j - 1]

    plt.figure(3)
    plt.clf()
    plt.plot(x, y, 'r-')

    plt.plot(xi, yi, 'go')
    plt.title("Arbitrary polynomial interpolation")
    filename = "poly-%s.png" %(n)
    plt.savefig(filename)


def plot_cubic(xi, yi):

    #Get the c coefficients
    c = cubic_interp(xi, yi)

    #Get the interpolation points
    x = np.linspace(xi.min() - 1, xi.max() + 1, 1000)
    y = c[0] + c[1] * x + c[2] * x**2 + c[3] * x**3

    plt.figure(2)
    plt.clf()
    plt.plot(x, y, 'b-') #plot polynomial

    plt.plot(xi, yi, 'ro') #plot data points
    plt.title("Data points and interpolating cubic polynomial")    
    plt.savefig("cubic.png")


def test_quad1():
    """
    Test code, no return value or exception if test runs properly.
    """
    xi = np.array([-1.,  0.,  2.])
    yi = np.array([ 1., -1.,  7.])
    c = quad_interp(xi,yi)
    c_true = np.array([-1.,  0.,  2.])
    print "c =      ", c
    print "c_true = ", c_true
    # test that all elements have small error:
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)


def test_quad2():
    """
    Second test, no return value or exception if test runs properly.
    """
    xi = np.array([9., 1., 91.])
    yi = 27 + 9 * xi + 91 * xi**2

    c = quad_interp(xi, yi)
    c_true = np.array([27., 9., 91.])
    
    print "c =      ", c                                                                                                 
    print "c_true = ", c_true                                                                               
 
    # test that all elements have small error:                                                                           
    assert np.allclose(c, c_true), "Incorrect result, c = %s, Expected: c = %s" % (c,c_true)    

    plot_quad(xi, yi)

def test_cubic1():
    """
    Test for the cubic polynomial
    """
    xi = np.array([-1., 1., 0., 2.])
    yi = 10 + 9 * xi + 8 * xi**2 + 7 * xi**3

    c = cubic_interp(xi, yi)
    c_true = np.array([10., 9., 8., 7.])

    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, c_true = %s " %(c, c_true)

    plot_cubic(xi, yi)

def test_poly1():
    """
    First test for n-polynomial
    """
    xi = np.array([1., 4., 3., 8.])
    c_true = [0.5, 0.7, 0.2, 0.8]

    #polyval gives in the reverse order
    c_true.reverse()
    p = np.array(c_true)
    yi = np.polyval(p, xi)

    c_true.reverse()
    c_true = np.array(c_true)

    c = poly_interp(xi, yi)

    plot_poly(xi, yi)

    print "c = ", c
    print "c_true = ", c_true
    
    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, c_true = %s " %(c, c_true)


def test_poly2():
    """                                                                                                                                              
    Second test for n-polynomial                                                                                                                     
    """
    xi = np.array([1., 4., 3., 8., 2.])
    c_true = [0.9, 1.7, 0.2, 0.3, 2.4]

    #polyval gives in the reverse order                                                                                                               
    c_true.reverse()
    p = np.array(c_true)
    yi = np.polyval(p, xi)

    c_true.reverse()
    c_true = np.array(c_true)

    c = poly_interp(xi, yi)

    plot_poly(xi, yi)

    print "c = ", c
    print "c_true = ", c_true

    assert np.allclose(c, c_true), \
        "Incorrect result, c = %s, c_true = %s " %(c, c_true)



if __name__=="__main__":
    # "main program"
    # the code below is executed only if the module is executed at the command line,
    #    $ python demo2.py
    # or run from within Python, e.g. in IPython with
    #    In[ ]:  run demo2
    # not if the module is imported.
    print "Running test..."
    test_quad1()
    test_quad2()
    test_cubic1()
    test_poly1()
    test_poly2()
