program intersections

  use newton
  use functions

  implicit none

  real(kind=8) :: x, x0, fx
  real(kind=8) :: x0vals(4)
  integer :: iters, itest
  logical :: debug

  debug = .true.

  x0vals = (/-4.d0, -1.5d0, -0.7d0, 1.4d0/)

  do itest=1,4

     x0 = x0vals(itest)
     print *, " "
     call solve(intersect, intersect_p, x0, x, iters, debug)
     
     print 11, x0
11   format("With initial guess x0 = ", es22.15)
     print 12, x, iters
12   format("solve returns x = ", es22.15, " after ", i3, " iterations")


     fx = intersect(x)

     if(debug) then
        print 13, fx
13      format("The value of fx is ", es22.15)
     endif

     if(abs(fx) > 1d-14) then
        print 14, x
14      format("Unexpected result: x = ", es22.15)
     endif

     enddo

end program intersections
