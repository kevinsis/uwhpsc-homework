! $UWHPSC/codes/fortran/newton/functions.f90

module functions

contains

real(kind=8) function f_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x

    f_sqrt = x**2 - 4.d0

end function f_sqrt


real(kind=8) function fprime_sqrt(x)
    implicit none
    real(kind=8), intent(in) :: x
    
    fprime_sqrt = 2.d0 * x

end function fprime_sqrt


real(kind=8) function intersect(x)
  implicit none
  real(kind=8), intent(in) :: x
  real(kind=8) :: pi

  pi = acos(-1.d0)

  intersect = x * cos(pi * x) - 1 + 0.6 * x * x

end function intersect


real(kind=8) function intersect_p(x)
  implicit none
  real(kind=8), intent(in) :: x
  real(kind=8) :: pi
  pi = acos(-1.d0)

  intersect_p = cos(pi * x) - pi * x * sin(pi * x) + 1.2 * x

end function intersect_p


end module functions
