from numpy import *
import matplotlib.pyplot as plt

import newton

x = linspace(-5, 5, 1000)

(g1, g1p) = newton.fvals_cos(x)
(g2, g2p) = newton.fvals_poly(x)

def fvals(x):
    f = x * cos(pi * x) - 1 + 0.6 * x**2
    fp = cos(pi * x) - pi * x * sin(pi * x) + 1.2 * x
    return (f, fp)

x_solve = array([0., 0., 0., 0.])

i = 0

for x0 in [-4., -1.5, -0.7, 1.4]:

    (x_solve[i], iters) = newton.solve(fvals, x0)
    print"With initial guess x0 = %22.15e" % x0
    print "solve returns x = %22.15e after %i iterations" %(x_solve[i], iters)
    i = i + 1

(y, dummy) = newton.fvals_cos(x_solve)

plt.figure(1)
plt.clf()
plt.plot(x, g1, 'b-', label='cos')
plt.plot(x, g2, 'g-', label='poly')
plt.plot(x_solve, y, 'ko')
plt.legend(('cos', 'poly'))
plt.savefig("intersections.png")
